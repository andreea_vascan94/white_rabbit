<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $coins = array(1, 2, 5, 10, 20, 50, 100);
		$coins_required = array(0, 0, 0, 0, 0, 0, 0); 
		$i = 6;
		while ($amount > 0) {
			$number_coins = floor($amount / $coins[$i]); 
			$coins_required[$i] = $number_coins;
			$amount -= $number_coins * $coins[$i];
			--$i;
		}
		return array(
			'1'   => $coins_required[0],
			'2'   => $coins_required[1],
			'5'   => $coins_required[2],
			'10'  => $coins_required[3],
			'20'  => $coins_required[4],
			'50'  => $coins_required[5],
			'100' => $coins_required[6]
		);
	}
    }
