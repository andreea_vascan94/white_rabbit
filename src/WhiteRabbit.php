<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {	
		return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        $text_file = file_get_contents($filePath);
		$text_array = str_split($text_file);
		$letters = "";
		foreach ($text_array  as $character) {
			if (ctype_alpha($character)){
				$letters = $letters.$character;
			}
		}
		return strtolower($letters);
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {	
		$look_up = array();
		$counted_letters = count_chars($parsedFile);
        for ($i = 0; $i < count($counted_letters); $i++) {
            if($counted_letters[$i]!= 0)
			    $look_up[] = array('letter'=>$i,'occurrences'=>$counted_letters[$i]); 
		}
		usort($look_up, function($a, $b) {
			return $a['occurrences'] <=> $b['occurrences'];
		});
		$occurrences = $look_up[round(count($look_up)/2)-1]['occurrences'];
        return chr($look_up[round(count($look_up)/2)-1]['letter']);

			
	}
}